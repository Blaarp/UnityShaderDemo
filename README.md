This was a fast demo to show how screen space shaders can be implemented in Unity

Shader examples:
InvertColors - Auto generated screen space shader that has been comented for better explaination. Basic as possible shader.
GaussianBlur - Quick gaussian blur implementation completed in two passes. Most straight forward example of one way to write multi pass shaders.

Materials:
Shows how the shaders are selected. They are the interface for communicating with the shaders.

Scripts:
Mover - Moves box around to add some dynamics to test the shader
GaussianController - Shows how to use temporary render textures to pass data from one pass to another
Shader controller - Demonstraights the simplist use of OnRenderImage

Objects:
Main Camera - contains both shader controller scripts. Toggle them individually to see the effect of either shader.