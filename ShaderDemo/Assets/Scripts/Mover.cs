﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 temp = transform.position;
        temp.y += Mathf.Sin(Time.realtimeSinceStartup) * .02f;
        temp.x += Mathf.Cos(Time.realtimeSinceStartup) * .02f;
        transform.position = temp;
	}
}
