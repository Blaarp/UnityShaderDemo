﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode] // allows previewing the shader without having to run
public class ShaderController : MonoBehaviour {

    public Material mat;

    // Set up for a single pass
    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(source, destination, mat);
    }
}
