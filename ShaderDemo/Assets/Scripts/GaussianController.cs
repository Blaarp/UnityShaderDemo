﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class GaussianController : MonoBehaviour {

    public Material GuassianBlur;
    [Range(0, 10)]
    public int passes = 1;

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Blur(source, destination, passes);
    }

    private void Blur(RenderTexture source, RenderTexture destination, int iterations)
    {
        // Temporary render textures, can be used to store images for longer periods of time
        // Can be used to pass data between multiple passes
        RenderTexture rt = RenderTexture.GetTemporary(source.width, source.height);
        RenderTexture rt2 = RenderTexture.GetTemporary(source.width, source.height);

        Graphics.Blit(source, rt);

        for (int i = 0; i < iterations; i++)
        {
            Graphics.Blit(rt, rt2, GuassianBlur, 0);
            Graphics.Blit(rt2, rt, GuassianBlur, 1);
        }

        Graphics.Blit(rt, destination);

        // Be sure to release these as you will run out of RAM very quickly
        RenderTexture.ReleaseTemporary(rt);
        RenderTexture.ReleaseTemporary(rt2);
    }
}
