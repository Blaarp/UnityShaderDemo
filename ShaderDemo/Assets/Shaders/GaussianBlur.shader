﻿Shader "Custom/GuassianBlur"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

			// Blur Horizontal
			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				#include "UnityCG.cginc"

				struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;

			}

			sampler2D _MainTex;
			float _BlurSize;
			float4 _MainTex_TexelSize;

			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 sum = fixed4(0.0,0.0,0.0,0.0);
				
				sum += tex2D(_MainTex, float2(i.uv.x - (_MainTex_TexelSize.x * 4.0), i.uv.y)) * 0.0162162162;
				sum += tex2D(_MainTex, float2(i.uv.x + (_MainTex_TexelSize.y * 4.0), i.uv.y)) * 0.0162162162;


				sum += tex2D(_MainTex, float2(i.uv.x - (_MainTex_TexelSize.x * 3.0), i.uv.y)) * 0.0540540541;
				sum += tex2D(_MainTex, float2(i.uv.x + (_MainTex_TexelSize.y * 3.0), i.uv.y)) * 0.0540540541;

				sum += tex2D(_MainTex, float2(i.uv.x - (_MainTex_TexelSize.x * 2.0), i.uv.y)) * 0.1216216216;
				sum += tex2D(_MainTex, float2(i.uv.x + (_MainTex_TexelSize.y * 2.0), i.uv.y)) * 0.1216216216;

				sum += tex2D(_MainTex, float2(i.uv.x - _MainTex_TexelSize.x, i.uv.y)) *  0.1945945946;
				sum += tex2D(_MainTex, float2(i.uv.x + _MainTex_TexelSize.y, i.uv.y)) *  0.1945945946;

				sum += tex2D(_MainTex, float2(i.uv.x, i.uv.y)) *  0.2270270270;

				return sum;
			}
			ENDCG
		}

		// Blur Vertical
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
		{
			float4 vertex : POSITION;
			float2 uv : TEXCOORD0;
		};

		struct v2f
		{
			float2 uv : TEXCOORD0;
			float4 vertex : SV_POSITION;
		};

		v2f vert(appdata v)
		{
			v2f o;
			o.vertex = UnityObjectToClipPos(v.vertex);
			o.uv = v.uv;
			return o;

		}

		sampler2D _MainTex;
		float4 _MainTex_TexelSize;

		fixed4 frag(v2f i) : SV_Target
		{
			fixed4 sum = fixed4(0.0,0.0,0.0,0.0);

			sum += tex2D(_MainTex, float2(i.uv.x, i.uv.y - (_MainTex_TexelSize.x * 4.0))) * 0.0162162162;
			sum += tex2D(_MainTex, float2(i.uv.x, i.uv.y + (_MainTex_TexelSize.y * 4.0))) * 0.0162162162;


			sum += tex2D(_MainTex, float2(i.uv.x, i.uv.y - (_MainTex_TexelSize.x * 3.0))) * 0.0540540541;
			sum += tex2D(_MainTex, float2(i.uv.x, i.uv.y + (_MainTex_TexelSize.y * 3.0))) * 0.0540540541;

			sum += tex2D(_MainTex, float2(i.uv.x, i.uv.y - (_MainTex_TexelSize.x * 2.0))) * 0.1216216216;
			sum += tex2D(_MainTex, float2(i.uv.x, i.uv.y + (_MainTex_TexelSize.y * 2.0))) * 0.1216216216;

			sum += tex2D(_MainTex, float2(i.uv.x, i.uv.y - _MainTex_TexelSize.x)) *  0.1945945946;
			sum += tex2D(_MainTex, float2(i.uv.x, i.uv.y + _MainTex_TexelSize.y)) *  0.1945945946;

			sum += tex2D(_MainTex, float2(i.uv.x, i.uv.y)) *  0.2270270270;

			return sum;
		}
			ENDCG
		}
	}
}
